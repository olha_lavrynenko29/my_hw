package MyProject;

public class ForDoughException {

    boolean flour;
    boolean salt;
    boolean water;
    boolean yeast;

    public void addFlour() {
        System.out.println("Flour added!");
        this.flour = true;
    }

    public void addSalt() {
        System.out.println("Salt added!");
        this.salt = true;
    }

    public void addWater() {
        System.out.println("Water added!");
        this.water = true;
    }

    public void done() throws TheDoughIsNotDoneRightException {
        System.out.println("Checked the ingredients for dough.");

        if (flour && salt && water && yeast) {
            System.out.println("The ingredients are right.");
        } else {
            throw new TheDoughIsNotDoneRightException("False! Checked your ingredients again!");
        }
    }
}
