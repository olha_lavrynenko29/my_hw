package MyProject;

public class CYeastMake extends CDoughIngredients {
    private int yeast;
    private int water;

    public CYeastMake(int water, int yeast) {
        super(water, yeast);
    }

    public int getYeast() {
        return yeast;
    }

    public int getWater() {
        return water;
    }

    public void makeYeast() {
        int myYeast = getWater() + getYeast();
        System.out.println("Add yeast to the ingredients for dough.");
    }
}
