package MyProject;

import java.util.ArrayList;
import java.util.List;

public class CMainCookingClass {
    public static void main(String[] args) {
        List<String> deliveryArea = new ArrayList<>();

        deliveryArea.add("Tairovo");
        deliveryArea.add("Poseloc Kotovskogo");
        deliveryArea.add("Cheremushki");
        deliveryArea.add("Center");

        System.out.println("Our delivery working in the " + deliveryArea.size() + " areas.");

        ForDoughException forDoughException = new ForDoughException();
        forDoughException.addFlour();
        forDoughException.addWater();
        forDoughException.addSalt();
        try {
            forDoughException.done();
        } catch (TheDoughIsNotDoneRightException e) {
            System.out.println(e.getMessage());
            System.out.println("Checked the dough: Flour? " + forDoughException.flour + " Salt? " + forDoughException.salt + " Water? " + forDoughException.water + " Yeast? " + forDoughException.yeast);
        }

        CDoughIngredients doughIngredients = new CDoughIngredients(200, 500, 50, 55);
        doughIngredients.takeIngredients();

        CYeastMake cYeastMake = new CYeastMake(50, 5);
        cYeastMake.makeYeast();
        doughIngredients.toMix();

        CTellTheDough tellTheDough = new CTellTheDough();
        tellTheDough.tellTheDough();

        CSauce sauce = new CSauce();
        sauce.makeSauce();

        CFilling cFilling = new CFilling();
        cFilling.dice();
        cFilling.grate();
        cFilling.putTheFilling();
        cFilling.margarita();
        for (TypeOfPizza Margarita : TypeOfPizza.values()) {
            System.out.println(Margarita);
        }
        cFilling.neopolitana();
        for (TypeOfPizza1 Neopolitana : TypeOfPizza1.values()) {
            System.out.println(Neopolitana);
        }

        CBakeClass bakeClass = new CBakeClass();
        System.out.println(bakeClass.turnOn());
        bakeClass.alarm();

        CSetTheTemperatureCookingTime cSetTheTemperature = new CSetTheTemperatureCookingTime();
        cSetTheTemperature.setTheTemperature();

        LastClass lastClass = new LastClass();
        lastClass.cut();
        lastClass.eat();
    }
}

