package MyProject;

public interface IToMix {
    void takeIngredients();

    void toMix();
}
