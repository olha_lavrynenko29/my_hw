package MyProject;

public class CBakeClass implements IOnOff, IForBakeClass {

    @Override
    public String turnOn() {
        return IOnOff.super.turnOn();
    }

    @Override
    public String turnOff() {
        return IOnOff.super.turnOff();
    }

    @Override
    public String bake() {
        return IOnOff.super.bake();
    }

    @Override
    public void alarm() {
        System.out.println("Turn off your bake for the 20 minutes.");
    }
}
