package MyProject;

public class CDoughIngredients implements IToMix {

    private int water;
    private int flour;
    private int salt;
    private int yeast;

    public CDoughIngredients() {
    }

    public CDoughIngredients(int water, int flour, int salt, int yeast) {
        this.water = water;
        this.flour = flour;
        this.salt = salt;
        this.yeast = yeast;
    }

    public CDoughIngredients(int water, int yeast) {
        this.water = water;
        this.yeast = yeast;
    }

    @Override
    public void takeIngredients() {
        System.out.println("Take all ingredients for dough.");
    }

    @Override
    public void toMix() {
        int all = water + yeast + salt + flour;
        System.out.println("Mix all our ingredients for dough.");
    }
}
