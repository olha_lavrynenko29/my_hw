package MyProject;

public class TheDoughIsNotDoneRightException extends Exception {

    public TheDoughIsNotDoneRightException(String message) {
        super(message);
    }
}
