package MyProject;

public class LastClass extends ADishes {
    @Override
    void cut() {
        super.cut();
        System.out.println("Cut your dish.");
    }

    @Override
    void eat() {
        super.eat();
        System.out.println("Eat and enjoy! You did it!)");
    }
}
