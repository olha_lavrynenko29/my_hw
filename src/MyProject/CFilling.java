package MyProject;

public class CFilling implements ICut {

    @Override
    public void dice() {
        System.out.println("Dice your products for filling.");
    }

    @Override
    public void grate() {
        System.out.println("And grate your cheese.");
    }

    void putTheFilling() {
        System.out.println("Put your filling on the dough.");
    }

    void margarita() {
        System.out.println("The Filling for pizza Margarita:");
    }

    void neopolitana() {
        System.out.println("The Filling for pizza Neopolitana:");
    }
}
