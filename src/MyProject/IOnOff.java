package MyProject;

public interface IOnOff {
    default String turnOn() {
        return "Turning the bake on.";
    }

    default String turnOff() {
        return "Turning the bake off.";
    }

    default String bake() {
        return "Bake your dish.";
    }

}
