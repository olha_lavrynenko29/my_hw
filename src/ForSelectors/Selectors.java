package ForSelectors;

public class Selectors {

    WebElement logo = driver.findElement(By.className(".logo--small"));
    WebElement logo1 = driver.findElement(By.xpath("//*[@class='page-header__wrapper']//*[@class='logo']"));

    WebElement button = driver.findElement(By.id("#signCoursesButton"));
    WebElement button1 = driver.findElement(By.xpath("//*[@id='signCoursesButton']"));

    WebElement map = driver.findElement(By.cssSelector(".map__inner img"));
    WebElement mapAlt = driver.findElements(By.xpath("//*[@class='map__inner']/img"));

    WebElement icon = driver.findElement(By.cssSelector("#sprite-svg #icon-Graduates"));
    WebElement icon1 = driver.findElements(By.xpath("//*[@id='icon-Graduates']"));

    WebElement course = driver.findElement(By.id("#course"));
    WebElement course1 = driver.findElements(By.xpath("//*[@id='course']"));

    WebElement testing = driver.findElement(By.cssSelector("#courseMenuNav [title='Тестирование']"));
    WebElement testing1 = driver.findElements(By.xpath("//*[@class='menu__item  ']//*[@class='menu__name']"));

    WebElement qaAutomation = driver.findElement(By.cssSelector(".menu__courses.active a:nth-child(2)"));
    WebElement qaAutomation1 = driver.findElements(By.xpath("//*[@class='menu__courses active']//a[2]"));

    WebElement skidka = driver.findElement(By.id("#magnet"));
    WebElement skidka1 = driver.findElements(By.xpath("//*[@id='magnet']"));

    WebElement name = driver.findElement(By.id("#inputMagnetName"));
    WebElement name2 = driver.findElements(By.xpath("//*[@id='inputMagnetName']"));

    WebElement email = driver.findElement(By.id("#inputMagnetEmail"));
    WebElement email2 = driver.findElements(By.xpath("//*[@id='inputMagnetEmail']"));

    WebElement button = driver.findElement(By.className(".modal__submit--disabled"));
    WebElement button2 = driver.findElements(By.xpath("//*[@id='buttonSubscribe']"));

    WebElement form = driver.findElement(By.id("#jcont"));
    WebElement form1 = driver.findElements(By.xpath("//*[@id='jcont']"));

    WebElement name3 = driver.findElement(By.cssSelector("jdiv.fieldWrap_26c input"));
    WebElement name4 = driver.findElements(By.xpath("/*[@class='fieldWrap_26c']//input[1]"));

    WebElement tel = driver.findElement(By.cssSelector("jdiv.fieldWrap_26c input[type='tel']"));
    WebElement tel1 = driver.findElements(By.xpath("//*[@id='jcont']//*[@type='tel'"));

    WebElement email3 = driver.findElement(By.cssSelector("jdiv.fieldWrap_26c input[type='email']"));
    WebElement email4 = driver.findElements(By.xpath("//*[@id='jcont']//*[@type='email']"));

    WebElement message = driver.findElement(By.cssSelector("jdiv.fieldWrap_26c [placeholder='Ваше сообщение*']"));
    WebElement message1 = driver.findElements(By.xpath("//*[@id='jcont']//*[@placeholder='Ваше сообщение*']"));

    WebElement button3 = driver.findElement(By.cssSelector("jdiv.button_df8"));
    WebElement button4 = driver.findElements(By.xpath("//*[@class='button_df8 _green_7cb']"));
}


