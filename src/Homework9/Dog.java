package Homework9;

public class Dog {

    private int paws;
    private String color;
    private int age;
    private String name;

    public Dog() {
    }

    public Dog(String color, String name) {
        this.color = color;
        this.name = name;
    }

    public Dog(int paws, String color, int age, String name) {
        this.paws = paws;
        this.color = color;
        this.age = age;
        this.name = name;
    }

    public int getPaws() {
        return paws;
    }


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getAge() {
        return age;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    void priceOfTheDog() {
        int priceOfTheDog = getAge() + getPaws();
        System.out.println("The price of the dog is " + priceOfTheDog);
    }

    void nameDog() {
        String nameDog = getName() + getColor();
        System.out.println("The real dog name is " + nameDog);
    }
}
