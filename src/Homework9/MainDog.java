package Homework9;

public class MainDog {
    public static void main(String[] args) {
        Dog dogs = new Dog(4, "black", 5, "Barbos");
        dogs.priceOfTheDog();
        dogs.nameDog();

        ShepherdDog shepherdDog = new ShepherdDog("Black", "Pups", "Germany");
        shepherdDog.setName("Sharyk");
        shepherdDog.nameDog();

        LapDog lapDog = new LapDog();
        lapDog.setName("Buffy");
        System.out.println("The LapDog name is " + lapDog.getName());

        BullDog bullDog = new BullDog("black", "Wolf", "Ukraine");
        bullDog.setColor("white");
        System.out.println("Change for bulldog is color: " + bullDog.getColor());
    }
}

