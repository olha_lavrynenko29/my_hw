package Homework9;

public class BullDog extends ShepherdDog {

    public BullDog(String color, String name, String country) {
        super(color, name, country);
        System.out.println("The ShepherdDog have the same color, name and country with BullDog");
    }
}
