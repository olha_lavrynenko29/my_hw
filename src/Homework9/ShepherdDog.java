package Homework9;

public class ShepherdDog extends Dog {

    private String country;


    public ShepherdDog(String color, String name, String country) {
        super(color, name);
        this.country = country;
    }

    @Override
    void nameDog() {
        super.nameDog();
        System.out.println("This dog is from " + country);
    }
}
