package CollectionExample;

public class ForArrayListException extends Exception {

    public ForArrayListException(String message) {
        super(message);
    }
}
