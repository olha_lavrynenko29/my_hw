package CollectionExample;

import java.util.*;

public class CollectionsExample {
    public static void main(String[] args) {

        List<Integer> numbersRandom = new ArrayList<>();
        Random random = new Random();
        while (numbersRandom.size() <= 10) {
            numbersRandom.add(random.nextInt(10));
        }
        System.out.println(numbersRandom);

        List<Integer> secondArrayList = new ArrayList<>(numbersRandom);
        System.out.println(secondArrayList);

        Set listUnique = new HashSet(secondArrayList);
        System.out.println(listUnique);

        List<Integer> number = new ArrayList<>(numbersRandom);

        Map<Integer, Integer> countMap = new HashMap<>();

        for (Integer numbers : numbersRandom) {
            if (countMap.containsKey(numbers))
                countMap.put(numbers, countMap.get(numbers) + 1);
            else
                countMap.put(numbers, 1);
        }
        System.out.println(countMap);

        List<Integer> sortNumber = new ArrayList<>(numbersRandom);
        sortNumber.sort(Comparator.naturalOrder());
        System.out.println(sortNumber);
        System.out.println("ArrayList Min Value: " + Collections.min(numbersRandom));
        System.out.println("ArrayList Max Value: " + Collections.max(numbersRandom));

        int sum = 0;
        for (int i : numbersRandom) {
            sum += i;
        }
        System.out.println("ArrayList Sum Value: " + sum);

        List<Integer> positiveNumber = new ArrayList<>(numbersRandom);
        Iterator<Integer> posNumberIterator = positiveNumber.iterator();
        while (posNumberIterator.hasNext()) {
            Integer posNumber = posNumberIterator.next();
            if (posNumber <= 0) {
                posNumberIterator.remove();
            }
        }
        System.out.println(positiveNumber);

        List<Integer> toRemoveNumber = new ArrayList<>(numbersRandom);
        Iterator<Integer> numberIterator = toRemoveNumber.iterator();
        while (numberIterator.hasNext()) {
            Integer nextNumber = numberIterator.next();
            if (nextNumber % 2 != 0) {
                numberIterator.remove();
            }
        }
        System.out.println(toRemoveNumber);

        try {
            List<Integer> numberSeven = new ArrayList<>(numbersRandom);
            if (!numberSeven.contains(7)) {
                throw new ForArrayListException("plohoe chislo.");
            }
            System.out.println("Contain 7");
        } catch (ForArrayListException e) {
            System.out.println(e.getMessage());
            System.out.println("Not contain 7");
        }
    }
}
