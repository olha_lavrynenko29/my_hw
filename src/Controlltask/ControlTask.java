package Controlltask;

import java.util.Scanner;

public class ControlTask {
    public static void main(String[] args) {
//        firstTaskIF();
//        firstTaskSwitch();
//        secondTaskForEach();
//        secondTaskFor();
//        secondTaskDoWhile();
//        thirdTask();
//        fourthTask();
        fifthTask();
//        sixthTask();
    }

    public static void firstTaskIF() {
        System.out.println("Введите число: ");
        Scanner scr = new Scanner(System.in);
        int number = scr.nextInt();

        if (number == 1) {
            System.out.println("Вы ввели число " + number);
        } else if (number == 2) {
            System.out.println("Вы ввели число " + number);
        } else {
            System.out.println("Вы ввели число " + number);
        }
    }

    public static void firstTaskSwitch() {
        System.out.println("Введите число: ");
        Scanner scr = new Scanner(System.in);
        int number = scr.nextInt();

        switch (number) {
            case 1:
                System.out.println("Вы ввели число 1");
                break;
            case 2:
                System.out.println("Вы ввели число 2");
                break;
            case 3:
                System.out.println("Вы ввели число  3");
                break;
            default:
                System.out.println("He вижу число!");
        }
    }

    public static void secondTaskForEach() {
        int arr[] = {4, 9, 15, 7};
        for (int element : arr) {
            System.out.print(" " + element);
        }
    }

    public static void secondTaskFor() {
        int arr[] = {4, 9, 15, 7};
        for (int i = 0; i < arr.length; i++) {
            System.out.print(" " + arr[i]);
        }
    }

    public static void secondTaskDoWhile() {
        int arr[] = {4, 9, 15, 7};
        int i = 0;
        do {
            System.out.print(" " + arr[i]);
            i++;
        } while (i < arr.length);
    }

    public static void thirdTask() {
        Scanner scr = new Scanner(System.in);
        System.out.println("Введите число:");
        int enter = scr.nextInt();
        int sum = 0 + enter;
        if (enter == 2) {
            System.out.println(sum);
        } else {
            int sum2 = 0;
            for (int i = 2; i <= enter; i++) {
                sum2 = sum2 + i;
            }
            System.out.println(sum2);
        }
    }

    public static void fourthTask() {
        Scanner scr = new Scanner(System.in);
        System.out.println("Возраст:");
        int age = scr.nextInt();

        System.out.println(age >= 18 ? "Проходи!" : "Сорян!");
    }

    public static void fifthTask() {
        String[] fruits = new String[4];
        fruits[0] = "apple";
        fruits[1] = "banana";
        fruits[2] = "orange";
        fruits[3] = "ananas";
        for (String i : fruits) {
            System.out.println(i);
        }
        int x = fruits.length;
        System.out.println(x);
        do {
            x--;
            System.out.println(fruits[x]);

        } while (x > 0);
    }


    public static void sixthTask() {
        int arr[][] = {{1, 7, 9, 10}, {5, 2, 2}};
        for (int i = arr.length - 1; i >= 0; i--) {
            for (int j = arr[i].length - 1; j >= 0; j--) {

                System.out.print(" " + arr[i][j]);
            }
        }
    }
}










