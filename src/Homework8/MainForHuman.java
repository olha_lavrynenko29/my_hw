package Homework8;

public class MainForHuman {
    public static void main(String[] args) {

        Human human = new Human(25, 56, 158, "blue");
        human.setWeight(12);
        human.setHeight(156);
        human.setEyesColor("dark blue");
        human.setAge(29);
        human.showMyIndex();
        human.showMyColor();
    }
}
