package Homework8;

public class Human {

    private int age;
    private int weight;
    private int height;
    private String eyesColor;


    public Human(int age, int weight, int height, String eyesColor) {
        this.age = age;
        this.weight = weight;
        this.height = height;
        this.eyesColor = eyesColor;
    }


    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getEyesColor() {
        return this.eyesColor;
    }

    public void setEyesColor(String eyesColor) {
        this.eyesColor = eyesColor;
    }


    int showMyIndex() {
        int myIndex = getAge() * (getWeight() + getHeight());
        System.out.println("The index of my body: " + myIndex);
        return myIndex;
    }

    void showMyColor() {
        String colorType = getEyesColor();
        System.out.println("My color type is: " + colorType);

    }
}
